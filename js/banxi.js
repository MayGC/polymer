$(function(){
	$.ajax({
		url : 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SP74665,SF61745,SF60634,SF43773/datos/oportuno?token=823eb2e1704d5564d77e4a04cba066ac488581c3ac39df8e4429a20282ec0382',
		jsonp : 'callback',
		dataType : 'jsonp', //Se utiliza JSONP para realizar la consulta cross-site
		success : function(response) {  //Handler de la respuesta
			var series=response.bmx.series;
			
			//Se carga una tabla con los registros obtenidos
			for (var i in series) {
				  var serie=series[i];
				  var reg='<tr><td>'+serie.titulo+'</td><td>'+serie.datos[0].fecha+'</td><td>'+serie.datos[0].dato+'</td></tr>'
				  $('#result').append(reg);
			}
		}
	});
});
